﻿using System;
using System.ComponentModel;

namespace Pear.Core
{

    /// <summary>
    /// 日志状态
    /// </summary>
    public enum LogStatus
    {
        /// <summary>
        /// 成功
        /// </summary>
        [Description("成功")]
        Success,

        /// <summary>
        /// 失败
        /// </summary>
        [Description("失败")]
        Failure
    }

    

}
