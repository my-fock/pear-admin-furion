using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Pear.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Pear.Application.SystemCenter
{
    /// <summary>
    /// 日志服务
    /// </summary>
    [ApiDescriptionSettings(ApiGroupConsts.SYSTEM_CENTER, Name = "logs")]
    public class LogService : ILogService, IDynamicApiController, ITransient
    {
        /// <summary>
        /// 内存缓存
        /// </summary>
        private readonly IMemoryCache _memoryCache;

        /// <summary>
        /// API访问日志仓储
        /// </summary>
        private readonly IRepository<LogApi> _logApiRepository;

        /// <summary>
        /// 登录日志仓储
        /// </summary>
        private readonly IRepository<LogLogin> _logLoginRepository;

        /// <summary>
        /// 操作日志仓储
        /// </summary>
        private readonly IRepository<LogOperate> _logOperateRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logApiRepository"></param>
        /// <param name="logLoginRepository"></param>
        /// <param name="logOperateRepository"></param>
        /// <param name="memoryCache"></param>
        public LogService(IRepository<LogApi> logApiRepository
            , IRepository<LogLogin> logLoginRepository
             , IRepository<LogOperate> logOperateRepository
            , IMemoryCache memoryCache)
        {
            _logApiRepository = logApiRepository;
            _logLoginRepository = logLoginRepository;
            _logOperateRepository = logOperateRepository;
            _memoryCache = memoryCache;
        }


        /// <summary>
        /// 获取所有API日志列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [SecurityDefine("admin.system.log:view"), HttpGet, ApiDescriptionSettings(Name = "logapilist")]
        public async Task<PagedList<LogApi>> GetLogApiListAsync([FromQuery, Required] GetLogApiListInput input)
        {
            var hasKeyword = !string.IsNullOrEmpty(input.Keyword?.Trim());

            var roles = await _logApiRepository.Where(
                                                (hasKeyword, u => EF.Functions.Like(u.Url, $"%{input.Keyword.Trim()}%")),
                                                (hasKeyword, u => EF.Functions.Like(u.Result, $"%{input.Keyword.Trim()}%")),
                                                (hasKeyword, u => EF.Functions.Like(u.Remark, $"%{input.Keyword.Trim()}%"))
                                              )
                                             .ToPagedListAsync(input.PageIndex, input.PageSize);

            return roles.Adapt<PagedList<LogApi>>();
        }

        /// <summary>
        /// 获取API日志信息
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.log:view"), HttpGet]
        public async Task<LogApi> LogApiProfileAsync([Required, Range(1, int.MaxValue, ErrorMessage = "请输入有效的日志 Id")] int logId)
        {
            // 查询日志是否存在
            var log = await _logApiRepository.FirstOrDefaultAsync(u => u.Id == logId, false);
            return log.Adapt<LogApi>();
        }



    }
}